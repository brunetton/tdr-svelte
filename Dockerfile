# Build ───────────────────────────────────────────────────────────────────────

# locally, you must clone

FROM node:12-alpine as build

RUN apk add --no-cache git
WORKDIR /build
RUN git clone --depth=1 https://framagit.org/brunetton/tdr-svelte.git .
RUN rm -rf .git
RUN npm install
RUN npm run build:tailwind
RUN npx sapper build
RUN npm install -g copy-node-modules

# Copy only usefull node_modules to /node_modules
RUN copy-node-modules . /node_modules
# Remove big and useless chromium image
RUN rm -rf /node_modules/node_modules/puppeteer/.local-chromium
RUN rm -rf /build/node_modules

# # Run ───────────────────────────────────────────────────────────────────────
FROM node:12-alpine

WORKDIR /app
COPY --from=build /build/__sapper__/build/ __sapper__/build/
# COPY --from=build /build/package.json __sapper__/build/
COPY --from=build /build/static/ static/
COPY --from=build /node_modules/node_modules/ __sapper__/build/node_modules/

ENV NODE_ENV=production PORT=3030
CMD node /app/__sapper__/build/

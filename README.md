## Launch app

- `npm run dev`
  - or
  - `docker run -p 3030:3030 --rm --name tdr tdr`
- open Firefox and navigate to http://localhost:3000
    - you've to give a `facebook token` to be logged in. To get one:
        - install [Tinder-token-firefox-extension](https://github.com/brunetton/tinder-token-firefox-extension)
            - optional: configure it to send back the token to the running app (see extension doc)
        - click on `login with Facebook` button, you'll be redirected to Facebook Oauth asking you to authorize application.
        - once clicked on "OK" the extensions intercept connection and display token, copy it to clipboard and send it back to running app
- you should now be able to use other tabs, like Updates

## Useful extensions

* [Open in browser](https://addons.mozilla.org/en-US/firefox/addon/open-in-browser/), to force opening jpeg with bad mime type in browser, not downloading them and ask for software to use

## Dev

### .env file

```
SAMPLES_MODE=1  # use samples (not real API)
DEBUG=1  # save recs results to file when in real mode
FACEBOOK_TOKEN=xxxxxxx
```

### Docker image build

```
git push  # needed, as Dokerfile clones the git repo from remote
docker rmi tdr:latest
docker system prune --volumes
docker build -t tdr:latest .
```

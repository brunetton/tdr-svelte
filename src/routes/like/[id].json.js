import moment from 'moment'
import fs from 'fs'

export async function get(req, res) {
    const id = req.params.id
    const superLike = req.query.super
    let returnMessage

    // Check if there's some [super]likes left
    let myself_metadata
    let client
    if (process.env.SAMPLES_MODE) {
        myself_metadata = require('../../../samples/me.json')
        myself_metadata.rating.super_likes.resets_at =
            '2019-10-01T02:00:28.166Z'
    } else {
        client = req.tinderClient
        myself_metadata = await client.getMetadata()
        if (process.env.DEBUG) {
            // save to file
            console.log(`Writing 'myself_metadata.json' file.`)
            fs.writeFile(
                'myself_metadata.json',
                JSON.stringify(myself_metadata),
                'utf8',
                (err) => {
                    if (err)
                        console.log(`can't write recs json file answer: ${err}`)
                }
            )
        }
    }

    let remainingBefore

    let error = false
    remainingBefore = superLike
        ? myself_metadata.rating.super_likes.remaining
        : myself_metadata.rating.likes_remaining
    console.log({ remainingBefore })
    if (remainingBefore >= 1 && !process.env.SAMPLES_MODE) {
        var answer
        if (superLike) {
            // Trigger super like
            console.log(`-> Superlike ${id}`)
            answer = await client.superLike(id)
        } else {
            // Trigger like
            console.log(`-> Like ${id}`)
            answer = await client.like(id)
        }
        console.log('* answer:', answer)
        if (superLike) {
            if (answer.status != 200) {
                returnMessage = `WARNING: Didn't worked ! (status ${answer.status})`
                error = true
            } else {
                returnMessage = 'OK :)'
            }
            if (answer.match) {
                returnMessage = "❤❤❤ WOW, it's a match :) ❤❤❤"
            }
        } else {
            if (answer.match) {
                returnMessage = "❤❤❤ WOW, it's a match :) ❤❤❤"
            } else {
                returnMessage = 'OK'
            }
        }
    } else if (process.env.SAMPLES_MODE) {
        await new Promise(r => setTimeout(r, 2000))
        if (superLike) {
            returnMessage = `Impossible: no more super likes ! Renew in ${moment
                .utc(myself_metadata.rating.super_likes.resets_at)
                .fromNow()}`
            error = true
        } else {
            returnMessage = `OK`
        }
    } else {
        returnMessage = `Impossible: no more super likes ! Renew in ${moment
            .utc(myself_metadata.rating.super_likes.resets_at)
            .fromNow()}`
        error = true
    }

    console.log(`  -> ${returnMessage}`)
    if (error) {
        console.log('* answer:', answer)
    }
    res.writeHead(200, {
        'Content-Type': 'application/json',
    })
    res.end(JSON.stringify({ error, text: returnMessage }))
}

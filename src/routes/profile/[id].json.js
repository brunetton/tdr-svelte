// import sample from '../../../samples/_profile.json.js'

export async function get(req, res) {
    let user
    if (process.env.SAMPLES_MODE) {
        user = require('../../../samples/profile.json')
        // user = require('../../../samples/profile_with_spotify3.json')
        user = {
            status: 200,
            results: user,
        }
    } else {
        const id = req.params.id
        const client = req.tinderClient
        if (!client) {
            res.end('no client')
            return
        }
        user = await client.getUser(id)
    }

    res.writeHead(200, {
        'Content-Type': 'application/json',
    })
    res.end(JSON.stringify(user))
}

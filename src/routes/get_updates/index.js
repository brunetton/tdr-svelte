import moment from 'moment'

export async function get(req, res) {
    let updates
    if (process.env.SAMPLES_MODE) {
        updates = require('../../../samples/updates.json')
        // updates = require('/home/bruno/dev/tdr/tdr-svelte/samples/updates.json')
    } else {
        const client = req.tinderClient
        if (!client) {
            res.end('no client')
            return
        }
        let fromDateStr =
            moment()
                .subtract(2, 'days')
                .format('YYYY-MM-DD') + 'T00:00:00.004Z' // Ex: "2020-01-28T00:00:00.004Z"
        updates = await client.getUpdates(fromDateStr)
    }

    // keep only inbox and matches and map them the number of new elements
    // Ex: { inbox: 0, matches: 6 }
    var _ = require('lodash')
    let filtered_updates = _.pick(updates, ['inbox', 'matches'])
    // filtered_updates = _.mapValues(filtered_updates, _.size)
    // console.log(filtered_updates)

    res.writeHead(200, {
        'Content-Type': 'application/json',
    })
    res.end(JSON.stringify(filtered_updates))
}

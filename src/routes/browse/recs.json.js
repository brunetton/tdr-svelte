import fs from 'fs'
export async function get(req, res) {
    let recs
    if (process.env.SAMPLES_MODE) {
        // recs = require('../../../samples/recommendations.json')
        recs = require('../../../samples/recommendations_error.json')
    } else {
        const client = req.tinderClient
        if (!client) {
            console.log('NO CLIENT')
            res.end('{}')
            return
        }
        recs = await client.getRecommendations()
    }
    let response = JSON.stringify(recs)
    if (!process.env.SAMPLES_MODE) {
        console.log({ response })
    }
    if (process.env.DEBUG) {
        // save to file
        fs.writeFile('last_recs.json', response, 'utf8', (err) => {
            if (err) console.log(`can't write recs json file answer: ${err}`)
        })
    }
    res.writeHead(recs.status && recs.status === 200 ? 200 : 503, {
        'Content-Type': 'application/json',
    })
    res.end(response)
}

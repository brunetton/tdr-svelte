export async function get(req, res) {
    const id = req.params.id
    let returnMessage
    let client
    let answer
    let error = false

    console.log('/pass')

    if (process.env.SAMPLES_MODE) {
        returnMessage = 'OK'
    } else {
        client = req.tinderClient
        answer = await client.pass(id)
        console.log('* answer:', answer)

        if (answer.status != 200) {
            returnMessage = `WARNING: Didn't worked ! (status ${answer.status})`
            error = true
        } else {
            returnMessage = 'OK'
        }
    }

    res.writeHead(200, {
        'Content-Type': 'application/json',
    })
    res.end(JSON.stringify({ error, text: returnMessage }))
}

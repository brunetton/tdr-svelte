export function post(req, res) {
    const token = req.query.token
    // console.log('------ token:', { token })

    if (token) {
        if (!RegExp('EAAG[a-zA-Z0-9]{200,}').test(token)) {
            const msg = `Invalid token: ${token}, doesn't match regex`
            console.log(msg)
            res.end(msg)
            return
        }
        console.log('set token:', token)
        process.env.newFacebookToken = req.query.token
        res.end('OK')
    }
}

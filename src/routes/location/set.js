import { select_option } from 'svelte/internal'

export async function get(req, res) {
    let api_res
    console.log(req.query.loc)

    var lat
    var lon
    ;[lat, lon] = req.query.loc.split(',')
    console.log({ lat, lon })
    if (!process.env.SAMPLES_MODE) {
        const client = req.tinderClient
        if (!client) {
            console.log('NO CLIENT')
            res.writeHead(503)
            res.end('NO CLIENT')
            return
        }
        console.log('avant')

        api_res = await client.changeLocation({
            latitude: lat,
            longitude: lon,
        })
        console.log('après')
    } else {
        api_res = { status: 200 }
    }
    let response = JSON.stringify(api_res)
    console.log({ response })

    res.writeHead(api_res.status && api_res.status === 200 ? 200 : 503, {
        'Content-Type': 'application/json',
    })
    res.end(response)
}

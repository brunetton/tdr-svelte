import sirv from 'sirv'
import polka from 'polka'
import compression from 'compression'
import * as sapper from '@sapper/server'
import td from 'tinder-client'

require('dotenv').config()

const { PORT, NODE_ENV, DEBUG, SAMPLES_MODE, FACEBOOK_TOKEN } = process.env
const dev = NODE_ENV === 'development'

let tinderClient
let facebookToken
process.env.newFacebookToken = FACEBOOK_TOKEN || ''

async function loglog(req, res, next) {
    console.log('\nlog - ', req.path)
    next()
}

async function tinderAuthMiddleware(req, res, next) {
    // res.send('Renew ton token')
    if (SAMPLES_MODE) {
        next()
        return
    }
    // console.log('tinderAuthMiddleware - req.path:', req.path)
    try {
        // console.log({ tinderClient })
        // console.log({ 'facebookToken': facebookToken })
        // Do not authenticate if already authenticated
        if (tinderClient) {
            // tinderClient is defined == we already auth successfully, just pass client to request
            req.tinderClient = tinderClient
        }
        if (process.env.newFacebookToken != '') {
            facebookToken = process.env.newFacebookToken
            process.env.newFacebookToken = ''
            console.log('newFacebookToken => do the auth')
            DEBUG && console.log('Logging into Tinder ...')
            console.log('Auth with', facebookToken)
            // eslint-disable-next-line require-atomic-updates
            tinderClient = await td.createClientFromFacebookAccessToken(
                facebookToken
            )
            // eslint-disable-next-line require-atomic-updates
            req.tinderClient = tinderClient
            console.log('-> auth OK')
        }
    } catch (err) {
        if (err.response.status == 401) {
            // Bad token
            console.log('Error 401 => bad token')
            // eslint-disable-next-line require-atomic-updates
            facebookToken = undefined
        } else if (err.response.status == 429) {
            // Too many requests
            res.end('Error 429 - Too many requests')
            return
        } else {
            console.log(
                '**************************',
                err.response.status,
                err.response.statusText,
                err
            )
            res.end(`Unexpected error ${err.status}, see logs`)
            return
        }
    }
    req.facebookToken = facebookToken
    next()
}

polka() // You can also use Express
    .use(
        compression({ threshold: 0 }),
        sirv('static', { dev }),
        tinderAuthMiddleware,
        sapper.middleware()
    )
    .listen(PORT, (err) => {
        if (err) console.log('error', err)
    })
